// https://dmco6fqramwh0.cloudfront.net/peur.mp4
export default {
  red: {
    abandonment: 'https://s3.eu-west-2.amazonaws.com/frqs-jdh/videos/peur.mp4',
    anger: 'https://s3.eu-west-2.amazonaws.com/frqs-jdh/videos/peur.mp4',
    disgust: 'https://s3.eu-west-2.amazonaws.com/frqs-jdh/videos/peur.mp4',
    doubt: 'https://s3.eu-west-2.amazonaws.com/frqs-jdh/videos/peur.mp4',
    threat: 'https://s3.eu-west-2.amazonaws.com/frqs-jdh/videos/peur.mp4',
    peur: 'https://s3.eu-west-2.amazonaws.com/frqs-jdh/videos/peur.mp4',
    sadness: 'https://s3.eu-west-2.amazonaws.com/frqs-jdh/videos/peur.mp4',
    violence: 'https://s3.eu-west-2.amazonaws.com/frqs-jdh/videos/peur.mp4',
    'joker-red': 'https://s3.eu-west-2.amazonaws.com/frqs-jdh/videos/peur.mp4',
  },
  orange: {
    action: 'https://s3.eu-west-2.amazonaws.com/frqs-jdh/videos/protection.mp4',
    courage:
      'https://s3.eu-west-2.amazonaws.com/frqs-jdh/videos/protection.mp4',
    movement:
      'https://s3.eu-west-2.amazonaws.com/frqs-jdh/videos/protection.mp4',
    patience:
      'https://s3.eu-west-2.amazonaws.com/frqs-jdh/videos/protection.mp4',
    preparation:
      'https://s3.eu-west-2.amazonaws.com/frqs-jdh/videos/protection.mp4',
    prevention:
      'https://s3.eu-west-2.amazonaws.com/frqs-jdh/videos/protection.mp4',
    protection:
      'https://s3.eu-west-2.amazonaws.com/frqs-jdh/videos/protection.mp4',
    unity: 'https://s3.eu-west-2.amazonaws.com/frqs-jdh/videos/protection.mp4',
    'joker-orange':
      'https://s3.eu-west-2.amazonaws.com/frqs-jdh/videos/protection.mp4',
  },
  green: {
    love: 'https://s3.eu-west-2.amazonaws.com/frqs-jdh/videos/confiance.mp4',
    calm: 'https://s3.eu-west-2.amazonaws.com/frqs-jdh/videos/confiance.mp4',
    confiance:
      'https://s3.eu-west-2.amazonaws.com/frqs-jdh/videos/confiance.mp4',
    energy: 'https://s3.eu-west-2.amazonaws.com/frqs-jdh/videos/confiance.mp4',
    estimedesoi:
      'https://s3.eu-west-2.amazonaws.com/frqs-jdh/videos/confiance.mp4',
    strength:
      'https://s3.eu-west-2.amazonaws.com/frqs-jdh/videos/confiance.mp4',
    joy: 'https://s3.eu-west-2.amazonaws.com/frqs-jdh/videos/confiance.mp4',
    peace: 'https://s3.eu-west-2.amazonaws.com/frqs-jdh/videos/confiance.mp4',
    'joker-green':
      'https://s3.eu-west-2.amazonaws.com/frqs-jdh/videos/confiance.mp4',
  },
};
